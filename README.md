### Code using javacript

#### Library
I use [`p5js`](https://p5js.org) library to draw things. It is reaaly interesting since it as a internal loop and a direct visual rendering. 

Please follow tutorial on javascript, `p5js` to get started. You can use the following links:
- Follow the installation and tutorial in https://p5js.org or use the online editor or codepen is you do not want to do that.
- https://thecodingtrain.com/Tutorials/ or the youtube channel [here](https://www.youtube.com/user/shiffman)
- https://www.youtube.com/watch?v=yPWkPOfnGsw This one is recent. and following video. This is about learning to code.
- There is a online editor: https://editor.p5js.org
- You can also use [codepen.io](https://codepen.io/) with a tutorial [here](https://www.youtube.com/watch?v=5gfUgNpS6kY&t=210s)
- html interaction basics https://www.youtube.com/watch?v=uNQSVU0IKec
- https://shiffman.net/a2z/intro/ (programmation de a à z)

- __A compléter__
```javascript
let nodes = [];
let r = 40;



function setup() {
	createCanvas(600,600);
  for (let i = 0; i < 100; i++){
  	let x = 300; // + random(-40,40);
    let y = 300; // + random(-40,40);
    let n = new node(x,y,i);
    nodes.push(n);
  }
  //noLoop();
  frameRate(10);
}

function draw() {
	background(0);
	fill(255);
	text(frameCount, width - 30, height - 30);
	finished = true;
  	for (let i = 0; i < nodes.length; i++){
	  	nodes[i].move();
  	}
  	for (let i = 0; i < nodes.length ; i++){
  		for (let j = i+1; j < nodes.length; j++){
	    		if (dist(nodes[i].x, nodes[i].y, nodes[j].x, nodes[j].y) < r + 1 ) {
		      		line(nodes[i].x, nodes[i].y, nodes[j].x, nodes[j].y);
		      	}
	    	}
	    	nodes[i].show();
  	}
}

function mousePressed(){
	redraw();
}

class node {
	constructor(x,y,i){
	this.x = x;
	this.y = y;
	this.id = i;
  }

  move() {
      this.x = this.x + random(-10,10) ;
      this.y = this.y + random(-10,10) ;
  }

  show (){
	stroke(255);
	strokeWeight(1);
	let c = map(this.id, 0, nodes.length, 0, 255);
	fill(c, c, c);
	ellipse(this.x, this.y, 10)
  }
}

```
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTE4MTE2NDEzMSwtODg3MTcxOTgxLC03Nz
Y4MjU0MDgsLTIwNDU4NzQ0ODksMjMzMjEzMzY0LDczNDIyNDkx
NiwxMDQ2NjUzODc2LC0yNzM5NTQ5NiwtMjEzNTMxODg0MCwtMj
A0MTg1NjA4MCwxNjI4MTg0Mjc5LC02MjAyNDU4OTYsLTEyMDE2
MjU5NDAsOTg1MTI0OTc2XX0=
-->