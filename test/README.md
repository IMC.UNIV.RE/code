```javascript
let nodes = [];
let r = 30;
let pos = [];
let finished = false;
let end = 0;


function setup() {
	createCanvas(600,600);
  pos = [PI/3, 2*PI/3, 3*PI/3, 4*PI/3, 5*PI/3, 6*PI/3];
  for (let i = 0; i < 10; i++){
  	let x = 300; // + random(-40,40);
    let y = 300; // + random(-40,40);
    let b = random(40,210);
    let n = new node(x,y,i,b);
    nodes.push(n);
  }
  //noLoop();
  frameRate(10);
}

function draw() {
  if (!finished){
		background(0);
		fill(255);
		text(frameCount, width - 30, height - 30);
		finished = true;
  	// clear();
  	for (let i = 0; i < nodes.length; i++){
    	if (random (0,1) < .1){
	  		nodes[i].move();
			}
		//nodes[i].show();
  	//nodes[i].move();
  	//nodes[i].show();
  	}
  	for (let i = 0; i < nodes.length ; i++){
  		for (let j = i+1; j < nodes.length; j++){
    		if (dist(nodes[i].x, nodes[i].y, nodes[j].x, nodes[j].y) < r + 1 ) {
      		line(nodes[i].x, nodes[i].y, nodes[j].x, nodes[j].y);
      	}
    		if (dist(nodes[i].x, nodes[i].y, nodes[j].x, nodes[j].y) < 1 ) {
      		line(nodes[i].x, nodes[i].y, nodes[j].x, nodes[j].y);
					finished = false;
					end = frameCount;
      	}
    	}
    	nodes[i].show();
  	}
	}
	
	if (finished){
		fill(255);
		//text(end, width - 60, height - 30);
		noLoop();
	}
}

function mousePressed(){
	redraw();
}

class node {
	constructor(x,y,i,b=100){
  	this.x = x;
    this.y = y;
    this.b = b;
    this.id = i;
  }

  move() {
    let mv = false;
		for (let i = 0; i < nodes.length; i ++){
    	if (this.id != i){
      	if (dist(this.x, this.y , nodes[i].x, nodes[i].y) < 1 && this.id > i){
        	mv = true;
        }
      }
    }

		if (mv){
    	let angle = PI;
      angle = pos[Math.floor(Math.random()*pos.length)];
      this.x = this.x + r*cos(angle) ;
      this.y = this.y + r*sin(angle) ;
      //this.x = this.x + random(-5,5) ;
      //this.y = this.y + random(-5,5) ;
    }

    if (this.x > width ){
    	this.x = 0;
    }
    if (this.x < 0 ){
    	this.x = width;
    }
    if (this.y > height ){
    	this.y = 0;
    }
    if (this.y < 0 ){
    	this.y = height;
    }
  }

  show (){
  	stroke(255);
    strokeWeight(.1);
    fill(this.b, this.id, this.id);
    ellipse(this.x, this.y, 7)
  }
}

```



```javascript
let nodes = [];
let r = 20;
let pos = [];


function setup() {
    createCanvas(600,600);
    pos = [PI/3, 2*PI/3, 3*PI/3, 4*PI/3, 5*PI/3, 6*PI/3];
    for (let i = 0; i < 200; i++){
        let x = 300; // + random(-40,40);
        let y = 300; // + random(-40,40);
        let b = random(10,250);
        let n = new node(x,y,i b);
        nodes.push(n);
    }
    //noLoop();
    frameRate(10);
}

function draw() {
    background(0);
    // clear();
    text(frameCount, width - 30, height - 30);
    for (let i = 0; i < nodes.length; i++){
    	if (random (0,1) < .1){
	   nodes[i].move();
	}
	//nodes[i].show();
        //nodes[i].move();
        //nodes[i].show();
    }
    for (let i = 0; i < nodes.length-1 ; i++){
        for (let j = i+1; j < nodes.length; j++){
            if (dist(nodes[i].x, nodes[i].y, nodes[j].x, nodes[j].y) < r + 1 ) {
                line(nodes[i].x, nodes[i].y, nodes[j].x, nodes[j].y);
            }
        }
        nodes[i].show();
    }
}

function mousePressed(){
    redraw();
}

class node {
    constructor(x,y,i,b=100){
        this.x = x;
        this.y = y;
        this.b = b;
        this.id = i;
    }

    move() {
        let mv = false;

        for (let i = 0; i < nodes.length; i ++){
            if (this.id != i){
                if (dist(this.x, this.y , nodes[i].x, nodes[i].y) < 1 && this.id > i){
                    mv = true;
                }
            }
        }

        if (mv){
            let angle = PI;
            angle = pos[Math.floor(Math.random()*pos.length)];
            this.x = this.x + r*cos(angle) ;
            this.y = this.y + r*sin(angle) ;
            //this.x = this.x + random(-5,5) ;
            //this.y = this.y + random(-5,5) ;
        }

        if (this.x > width ){
            this.x = 0;
        }
        if (this.x < 0 ){
            this.x = width;
        }
        if (this.y > height ){
            this.y = 0;
        }
        if (this.y < 0 ){
            this.y = height;
        }
    }

    show (){
        stroke(255);
        strokeWeight(.1);
        fill(this.b, this.id, this.id);
        ellipse(this.x, this.y, 7)
    }

}

```

The html file is like:
```html
<html>
<head>
  <meta charset="UTF-8">
  <script language="javascript" type="text/javascript" src="../p5.js"></script>
  <!-- uncomment lines below to include extra p5 libraries -->
  <!--<script language="javascript" src="libraries/p5.dom.js"></script>-->
  <!--<script language="javascript" src="libraries/p5.sound.js"></script>-->
  <script language="javascript" type="text/javascript" src="sketch.js"></script>
  <!-- this line removes any default padding and style. you might only need one of these values set. -->
  <style> body {padding: 0; margin: 0;} </style>
</head>

<body>
</body>
</html>
```

<!--stackedit_data:
eyJoaXN0b3J5IjpbLTQyMjkwNjAxMywxMDI5MDIzNTc0XX0=
-->